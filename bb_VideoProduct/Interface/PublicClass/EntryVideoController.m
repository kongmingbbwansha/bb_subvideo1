//
//  EntryVideoController.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/21.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "EntryVideoController.h"
#import "BaseControl.h"
#import "VideoPlayView.h"
#import "Videoslider.h"

typedef enum
{
    VideoStop = 1, //播放状态
    VideoPlay = 2, //暂停状态
    
}VideoPlayState;

@interface EntryVideoController ()

@property(copy,nonatomic)NSString *url;

@property(assign,nonatomic) BOOL ISWithShow;

/**
 *  视频view
 */
@property (strong, nonatomic) VideoPlayView  * VideoPlayer;

/**
 *  音频播放器
 */
@property (strong, nonatomic) AVPlayer       * AVplayer;

/**
 *  播放源
 */
@property (strong, nonatomic) AVPlayerItem   * playerItem;
/**
 *  功能条
 */
@property (strong, nonatomic) UIView         * ToolviewUP;
@property (strong, nonatomic) UIView         * ToolviewDown;
/**
 *  返回按钮
 */
@property (strong, nonatomic) UIButton       * backbtn;

/**
 *  播放暂停按钮
 */
@property (strong, nonatomic) UIButton       * Playbtn;

/**
 *  弹出弹框或者取消弹框
 */
@property(strong,nonatomic) UIButton         * popup;
/**
 *  全屏按钮
 */
@property (strong, nonatomic) UIButton       * Screenbtn;

/**
 *  进度条
 */
@property (strong, nonatomic) Videoslider    * Slider;

/**
 *  滚动栏
 */
@property (strong, nonatomic) UIScrollView   * ScrollView;

/**
 *  标题
 */
@property (strong, nonatomic) UILabel        * Titlelabel;

/**
 *  背景view
 */
@property (strong, nonatomic) UIView         * Backview;

/**
 *  暂停或者播放
 */
@property (assign ,nonatomic) BOOL             IS_Play;

/**
 *  定时器,监听播放
 */
@property (strong, nonatomic) NSTimer        * timer;
/**
 *  检测是否为缓冲状态
 */
@property (strong, nonatomic) NSTimer        * bufferTimer;

/**
 *  视频播放状态
 */
@property (assign, nonatomic) VideoPlayState   VideoState;

/**
 *  加载提示
 */
@property (strong, nonatomic) UIActivityIndicatorView * activity;

/**
 *  隐藏视频播放条定时器
 */
@property (strong, nonatomic) NSTimer        * hidetimer;
/**
 *  播放时间
 */
@property (strong, nonatomic) UILabel        * HeadTime;
/**
 *  剩余时间
 */
@property (strong, nonatomic) UILabel        * FooterTime;
/**
 * 播放时间
 */
@property (strong, nonatomic) UILabel        * Hlable;



@end

@implementation EntryVideoController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navgationbar.hidden=YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
   
    self.url = [BaseControl MP4URLvid:self.video_id];
    self.ISWithShow = YES;
    //布局
    [self layout];
    [self performSelector:@selector(isOnDiloadVideo) withObject:nil afterDelay:15.0f];
    //退后台和进入前台
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(videoStop) name:@"stop" object:nil];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(videoplay) name:@"play" object:nil];
    
}

-(void)layout
{
    //视频播放器
    self.VideoPlayer=[[VideoPlayView alloc]init];
    [self.view addSubview:self.VideoPlayer];
    self.VideoPlayer.backgroundColor = [UIColor blackColor];
    self.VideoPlayer.sd_layout.leftSpaceToView(self.view,0).rightSpaceToView(self.view,0).topSpaceToView(self.view,0).bottomSpaceToView(self.view,0);
    
    //监听点击事件，弹出或者取消弹框
     self.popup=[[UIButton alloc]init];
    [self.view addSubview:self.popup];
    self.popup.sd_layout.leftSpaceToView(self.view,0).rightSpaceToView(self.view,0).topSpaceToView(self.view,50).bottomSpaceToView(self.view,50);
    [self.popup addTarget:self action:@selector(toolstate) forControlEvents:UIControlEventTouchUpInside];
    
    //设置上下工具栏
    self.ToolviewUP=[[UIView alloc]init];
    self.ToolviewDown=[[UIView alloc]init];
    [self.VideoPlayer addSubview:self.ToolviewUP];
    [self.VideoPlayer addSubview:self.ToolviewDown];
    self.ToolviewUP.sd_layout.leftSpaceToView(self.VideoPlayer,0).rightSpaceToView(self.VideoPlayer,0).topSpaceToView(self.VideoPlayer,0).heightIs(50);
    self.ToolviewDown.sd_layout.leftSpaceToView(self.VideoPlayer,0).rightSpaceToView(self.VideoPlayer,0).bottomSpaceToView(self.VideoPlayer,0).heightIs(50);
    self.ToolviewUP.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
    self.ToolviewDown.backgroundColor=[UIColor colorWithWhite:0.000 alpha:0.500];
    
   //返回按钮
    self.backbtn=[[UIButton alloc]init];
    [self.ToolviewUP addSubview:self.backbtn];
    self.backbtn.sd_layout.bottomSpaceToView(self.ToolviewUP,5).leftSpaceToView(self.ToolviewUP,5).leftSpaceToView(self.ToolviewUP,15).widthIs(40);
    [self.backbtn setImage:[UIImage imageNamed:@"v3-btn-player-back@2x"] forState:UIControlStateNormal];
    [self.backbtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    //设置标题
    self.Hlable=[[UILabel alloc]init];
    [self.ToolviewUP addSubview:self.Hlable];
    self.Hlable.sd_layout.leftSpaceToView(self.ToolviewUP,100).rightSpaceToView(self.ToolviewUP,100).topSpaceToView(self.ToolviewUP,15).topSpaceToView(self.ToolviewUP,15);
    self.Hlable.textColor=[UIColor whiteColor];
    self.Hlable.font=[UIFont fontWithName:@"Helvetica" size:18];
    self.Hlable.text=self.Htitle;
    //文字居中显示 2为靠做 3 为靠右
    self.Hlable.textAlignment=1;
    
    //暂停播放按钮
    self.Playbtn=[[UIButton alloc]init];
    [self.ToolviewDown addSubview:self.Playbtn];
    self.Playbtn.sd_layout.bottomSpaceToView(self.ToolviewDown,5).leftSpaceToView(self.ToolviewDown,5).leftSpaceToView(self.ToolviewDown,15).widthIs(40);
    [self.Playbtn setImage:[UIImage imageNamed:@"v3-btn-player-pause@2x"] forState:UIControlStateNormal];
    [self.Playbtn addTarget:self action:@selector(playAndPause) forControlEvents:UIControlEventTouchUpInside];
    
    //进度条
    self.Slider = [[Videoslider alloc]init];
    [self.ToolviewDown addSubview:self.Slider];
    self.Slider.sd_layout.leftSpaceToView(self.ToolviewDown,120).rightSpaceToView(self.ToolviewDown,120).topSpaceToView(self.ToolviewDown,20).bottomSpaceToView(self.ToolviewDown,20);
    //进度条未播放部分颜色
    self.Slider.maximumTrackTintColor = [UIColor colorWithWhite:0.000 alpha:0.700];
    self.Slider.minimumTrackTintColor = [UIColor yellowColor];
    UIImage *schedule=[UIImage imageNamed:@"v3-player-slider@2x"];
    [self.Slider setThumbImage:schedule forState:UIControlStateNormal];
    [self.Slider setThumbImage:schedule forState:UIControlStateHighlighted];
    //滑动滑块触发方法
    [self.Slider addTarget:self action:@selector(sliderAction:) forControlEvents:UIControlEventValueChanged];
    
    //播放时间
    self.HeadTime=[[UILabel alloc]init];
    [self.ToolviewDown addSubview:self.HeadTime];
    self.HeadTime.sd_layout.leftSpaceToView(self.Playbtn,15).rightSpaceToView(self.Slider,15).topSpaceToView(self.ToolviewDown,35/2).heightIs(15);
    self.HeadTime.text=@"0:00";
    self.HeadTime.textColor=[UIColor whiteColor];
    self.HeadTime.font=[UIFont fontWithName:@"Helvetica" size:12];
 
    //剩余时间
    self.FooterTime=[[UILabel alloc]init];
    [self.ToolviewDown addSubview:self.FooterTime];
    self.FooterTime.sd_layout.leftSpaceToView(self.Slider,15).rightSpaceToView(self.ToolviewDown,70).topSpaceToView(self.ToolviewDown,35/2).heightIs(15);
    self.FooterTime.text=@"0:00";
    self.FooterTime.font=[UIFont fontWithName:@"Helvetica" size:12];
    self.FooterTime.textColor=[UIColor whiteColor];

    //加载UI
    self.activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
     [self.view addSubview:self.activity];
//    [self.activity setCenter:CGPointMake((self.view.frame.size.height -50)/2, (self.view.frame.size.width -50)/2)];
   
 
    //视频加载
    [self VideoLoad];
    
}
#pragma mark - 创建播放器
-(void)VideoLoad
{
    NSLog(@"%@",self.url);
    
    NSURL *url=[NSURL URLWithString:self.url];
    self.playerItem = [AVPlayerItem playerItemWithURL:url];
    [self.AVplayer replaceCurrentItemWithPlayerItem:nil];
    self.AVplayer = [AVPlayer playerWithPlayerItem:self.playerItem];
    AVPlayerLayer * Layer = (AVPlayerLayer *)self.VideoPlayer.layer;
    [Layer setPlayer:self.AVplayer];
    [self.AVplayer play];
    self.IS_Play=YES;
 
    //间隔10秒后隐藏工具栏，
    self.hidetimer=[NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(ToolviewISHiden) userInfo:nil repeats:NO];
    //每0.5秒钟更新时间和滚动条状态
    self.timer=[NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(UpTibleAndSlider) userInfo:nil repeats:YES];
    //检测视频是否为缓冲状态
    self.bufferTimer=[NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(isVideoPlayState) userInfo:nil repeats:YES];
}
/**
 * 判断视频是否加载成功
 */
-(void)isOnDiloadVideo
{
    if ([self presentTime]<1) {
        NSLog(@"你的网络太慢了,即将退出");
        UIAlertController *alear=[UIAlertController alertControllerWithTitle:@"提示" message:@"您的网络太慢了" preferredStyle:UIAlertControllerStyleAlert];
        [alear addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self back];
        }]];
        [self presentModalViewController:alear animated:YES];
    }
}

/**
 * 显示广告
 */
-(void)show
{
    [[[AppSingleton initAppsingleton] interstitialObj] presentFromRootViewController:self];
}

#pragma mark -加载广告
-(void)interstitialObjload
{
    //插屏广告
    if ([AppSingleton initAppsingleton].haveWithAdevert==0)
    {
        [[AppSingleton initAppsingleton] AdvertPrestrain];
    }
    
        
    
    
}

#pragma mark - 隐藏或者显示工具栏
-(void)toolstate
{
    if (self.ToolviewUP.hidden==NO)
    {
        self.ToolviewUP.hidden=YES;
        self.ToolviewDown.hidden=YES;
        [self.hidetimer invalidate];
    }
    else
    {
        self.ToolviewUP.hidden=NO;
        self.ToolviewDown.hidden=NO;
        self.hidetimer=[NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(ToolviewISHiden) userInfo:nil repeats:NO];
    }
}

-(void)back
{

    if ([AppSingleton initAppsingleton].haveWithAdevert == 0)
    {
        
        [[AppSingleton initAppsingleton] AdvertPrestrain];
    }

    self.VideoState = VideoStop;
    [self.AVplayer pause];
    [self.timer invalidate];
    [self.bufferTimer invalidate];
    [self dismissViewControllerAnimated:YES completion:nil ];

}

#pragma mark - touch方法
/**
 * 点击暂停按钮
 */
-(void)playAndPause
{
    [self.hidetimer invalidate];
    self.hidetimer=[NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(ToolviewISHiden) userInfo:nil repeats:NO];
    self.IS_Play = !self.IS_Play;
}

/**
 * 暂停或播放
 */
-(void)setIS_Play:(BOOL)IS_Play
{
    _IS_Play = IS_Play;
    if (IS_Play == YES)
    {
        [self.Playbtn setImage: [UIImage imageNamed:@"v3-btn-player-pause"] forState:UIControlStateNormal];
        [self.AVplayer play];
    }
    else
    {
        [self.Playbtn setImage: [UIImage imageNamed:@"v3-btn-player-play"] forState:UIControlStateNormal];
        [self.AVplayer pause];
    }
}

/**
 * 移动滑块，调整视频
 */
-(void)sliderAction:(UISlider*)sender
{
    //value为0-1的小数，0为刚开始，1为结束
    [self.AVplayer seekToTime:CMTimeMake(sender.value * [self AllTime], 1)];
    //滑动滑块的时候重置工具栏消失时间
    [self.hidetimer invalidate];
    self.hidetimer=[NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(ToolviewISHiden) userInfo:nil repeats:NO];
}

#pragma mark - 更新滑块状态播放时间和剩余时间
-(void)UpTibleAndSlider
{
    self.Slider.value = [self presentTime]/[self AllTime];
    NSString * left=[self Lookvideotimr:[self presentTime]];
    NSString * right=[self Lookvideotimr:([self AllTime]-[self presentTime])];
    if ([self presentTime] > 0 && [self AllTime] > 0)
    {
        [self.HeadTime setText:left];
        [self.FooterTime setText:right];
    }
    if ([self AllTime] > 0)
    {
        //视频播放完毕，退出视频播放
        if ([self AllTime] < ([self presentTime] + 1))
        {
            [self back];
        };
    }
    
    if (self.ISWithShow)
    {
        if ([AppSingleton initAppsingleton].interstitialObj.isReady==YES)
        {
            [self show];
        }
        self.ISWithShow=NO;
    }
}

#pragma mark - 视频数据获取方法
/**
 * timer设置格式
 */
-(NSString *)Lookvideotimr:(NSInteger )timer
{
    NSString * AllTime;
    NSInteger time = timer;
    if ((time - time/60 * 60 < 10 && time/60!=0) || time < 10)
    {
        AllTime = [NSString stringWithFormat:@"%ld:0%ld",(long)time/60,(long)(time - time/60 * 60)];
    }
    else
    {
        AllTime = [NSString stringWithFormat:@"%ld:%ld",(long)time/60,(long)(time - time/60 * 60)];
    }
    return AllTime;
}

/**
 * 视频总时长
 */
-(NSInteger)AllTime
{
    CMTime duration = self.AVplayer.currentItem.duration;
    NSInteger seconds = CMTimeGetSeconds(duration);
    return seconds;
}

/**
 * 视频当前的播放时间
 */
-(CGFloat )presentTime
{
    CGFloat time = 0;
    time = CMTimeGetSeconds(self.AVplayer.currentItem.currentTime);
    return time;
}

/**
 * 隐藏上边工具栏
 */
-(void)ToolviewISHiden
{
    if (self.ToolviewDown.hidden==NO)
    {
        self.ToolviewUP.hidden=YES;
        self.ToolviewDown.hidden=YES;
    }
}

/**
 * 监听是播放状态还是缓冲状态
 */
-(void)isVideoPlayState
{
    if ((2 + [self presentTime] < [self availableDuration] && self.IS_Play==YES) || ([self AllTime] == [[NSString stringWithFormat:@"%.0f",[self availableDuration]]integerValue]&&[self AllTime]!= 0))
    {
        
        [self.activity stopAnimating];
     }
    else
    {
        [self.activity setCenter:self.VideoPlayer.center];
        [self.activity startAnimating];
            }
    if (self.IS_Play==YES)
    {
        self.IS_Play=YES;
    }
}

/**
 * 当前缓冲时间
 */
- (CGFloat)availableDuration
{
    NSArray *loadedTimeRanges = [self.playerItem loadedTimeRanges];
    CMTimeRange timeRange = [loadedTimeRanges.firstObject CMTimeRangeValue];// 获取缓冲区域
    float startSeconds = CMTimeGetSeconds(timeRange.start);
    float durationSeconds = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result = startSeconds + durationSeconds;// 计算缓冲总进度
    return result;
}
/**
 *  挂后台
 */
-(void)videoStop
{
    if (self.VideoState==VideoPlay) {
        if (self.IS_Play == YES)
        {
            self.VideoState = VideoPlay;
            
        }
        else
        {
            self.VideoState = VideoStop;
        }
        self.IS_Play = NO;
    }
    
}

/**
 * 进入前台
 */
-(void)videoplay
{
    
    
    if ([AppSingleton initAppsingleton].insetadvert == 1)
    {
        [self dismissViewControllerAnimated:YES completion:^{
            [AppSingleton initAppsingleton].insetadvert = 0;
        }];
    }

    if (self.VideoState == VideoPlay)
    {
        self.IS_Play = YES;
    }
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    return YES;
    
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskLandscapeRight;
}

@end

//
//  AppSingleton.m
//  bb_Picturebook
//
//  Created by 符之飞 on 16/3/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "AppSingleton.h"

@interface AppSingleton ()
@property (assign ,nonatomic) int NumberWithAdvert;
@end

@implementation AppSingleton

static  AppSingleton * App;

+(AppSingleton *)initAppsingleton
{
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        App = [[AppSingleton alloc] init];
        
    });
    return App;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self AdvertPrestrain];
    }
    return self;
}
#pragma mark - 广告预加载
-(void)AdvertPrestrain
{
    //预加载广告
    _interstitialObj = [[GDTMobInterstitial alloc] initWithAppkey:APPkey placementId:IensertplacementId];
    _interstitialObj.delegate = self; //设置委托
    _interstitialObj.isGpsOn = NO; //【可选】设置GPS开关
    //预加载广告
    [_interstitialObj loadAd];
}

/**
 *  广告预加载成功回调
 *  详解:当接收服务器返回的广告数据成功后调用该函数
 */
- (void)interstitialSuccessToLoadAd:(GDTMobInterstitial *)interstitial
{
    self.haveWithAdevert=1;
    NSLog(@"插屏广告加载成功");
}

//广告加载失败
- (void)interstitialFailToLoadAd:(GDTMobInterstitial *)interstitial error:(NSError *)error
{

       self.haveWithAdevert=0;
         NSLog(@"插屏广告加载失败");

    
}

/**
 *  插屏广告展示结束回调
 *  详解: 插屏广告展示结束回调该函数
 */
- (void)interstitialDidDismissScreen:(GDTMobInterstitial *)interstitial
{

     self.haveWithAdevert=0;
   
    [self AdvertPrestrain];
     self.insetadvert=0;
}

/**
 *  插屏广告视图展示成功回调
 *  详解: 插屏广告展示成功回调该函数
 */
- (void)interstitialDidPresentScreen:(GDTMobInterstitial *)interstitial
{
    NSLog(@"点击了00000000");
     self.insetadvert=1;
}

/**
 *  插屏广告点击时回调
 *  详解: 插屏广告点击时回调
 */
-(void)interstitialClicked:(GDTMobInterstitial *)interstitial
{
    
    
    self.insetadvert = 1;
   
}
/**
 *
 *  进入后台调用
 */

-(void)interstitialApplicationWillEnterBackground:(GDTMobInterstitial *)interstitial
{



}

@end

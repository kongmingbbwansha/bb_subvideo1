//
//  EntryVideoController.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/21.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "BaseViewController.h"
#import <UIKit/UIKit.h> 
#import "GDTMobInterstitial.h"
@interface EntryVideoController : BaseViewController
@property(copy,nonatomic) NSString *video_id;
@property(copy,nonatomic) NSString *Htitle;
@end

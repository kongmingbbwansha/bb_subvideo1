//
//  InetermadiateViewController.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/19.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "InetermadiateViewController.h"
#import "EntryViewTableViewCell.h"
#import "EntryRequest.h"
#import "EntryModel.h"
#import "EntryVideoController.h"

@interface InetermadiateViewController ()<UITableViewDataSource,UITableViewDelegate>
/**
 * tableView
 */
@property(strong,nonatomic) UITableView *    tableView;

/**
 * tableView的赋值数组
 */
@property(copy,nonatomic)   NSMutableArray *  tableViewArray;
/**
 * 加载数据的参数
 */
@property(assign,nonatomic) int page;


@end

@implementation InetermadiateViewController
-(NSMutableArray *)tableViewArray
{
    if (_tableViewArray==nil) {
        _tableViewArray=[NSMutableArray array];
        self.page=1;
    }
    return _tableViewArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
       
    self.title=@"高三";
    [self layout];
    [self GET_Data];
}
/**
 * 布局
 */
-(void)layout
{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
    [self.view addSubview:self.tableView];
    self.tableView.sd_layout.topSpaceToView(self.navgationbar,0).bottomSpaceToView(self.view,49).widthIs(self.view.frame.size.width);
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.tableView.backgroundColor=[UIColor colorWithWhite:0.933 alpha:1.000];
     //下拉了刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(GET_Data)];
    self.tableView.mj_header = header;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(GET_tagMore)];
    [footer setTitle:@"没有更多内容了" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
}
#pragma mark - 加载数据
/**
 * 获取数据
 */
-(void)GET_Data
{
    self.page=1;
    
    EntryRequest *entry=[[EntryRequest alloc]initWithRequest:GET_subapp];
    entry.tab=@"3";
    entry.app_id=app_id;
    entry.app_sub_id=app_sub_id;
    entry.page=[NSString stringWithFormat:@"%d",self.page];
    __weak __typeof (self)weakself = self;
    [entry GetConectwithSucceed:^(id object) {
        __strong __typeof(self)strongself = weakself;
        [strongself.tableView.mj_header endRefreshing];
        if (SSUCCESS(object))
        {
            
            [strongself BannerObjectdata:[object objectForKey:@"data"]];
            
        }
        
    } Failure:^(NSError *failure) {
        __strong __typeof(self)strongself = weakself;
        [strongself.tableView.mj_header endRefreshing];
    }];
}
/**
 * 加载更多数据
 */
-(void)GET_tagMore
{
    self.page++;
    EntryRequest *entry=[[EntryRequest alloc]initWithRequest:GET_subapp];
    entry.tab=@"3";
    entry.app_id=app_id;
    entry.app_sub_id=app_sub_id;
    entry.page=[NSString stringWithFormat:@"%d",self.page];
    __weak __typeof (self)weakself = self;
    [entry GetConectwithSucceed:^(id object) {
        __strong __typeof(self)strongself = weakself;
        if (SSUCCESS(object))
        {
            [strongself ListObjectdataMore:[object objectForKey:@"data"]];
            
        }
        
    }
                        Failure:^(NSError *failure)
     {
         __strong __typeof(self)strongself = weakself;
         [strongself.tableView.mj_footer endRefreshing];
     }];
    
}
#pragma mark - 数据赋值
/**
 * 加载更多数据赋值方法
 */
-(void)ListObjectdataMore:(NSArray *)array
{
    if (array.count)
    {
        for (NSDictionary * dic in array)
        {
            EntryModel * model = [[EntryModel alloc]initWithDic:dic];
            [self.tableViewArray addObject:model];
        }
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
    }
    else
    {
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
    }
}


/**
 *记载更多的赋值
 */
-(void)BannerObjectdata:(NSArray *)array
{
    [self.tableViewArray removeAllObjects];
    for (NSDictionary * dic in array)
    {
        EntryModel * model = [[EntryModel alloc]initWithDic:dic];
        [self.tableViewArray addObject:model];
    }
    //下拉刷新重置上拉刷新的状态
    self.tableView.mj_footer.state=MJRefreshStateIdle;
    [self.tableView reloadData];
    
}
#pragma mark - tableView
/**
 * tableView
 */
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableViewArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{   
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EntryViewTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"zj"];
    if (cell==nil) {
        cell= [[EntryViewTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"zj"];
    }
    EntryModel *model=self.tableViewArray[indexPath.section];
    [cell DataPus:model];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EntryVideoController *video=[[EntryVideoController alloc]init];
    EntryModel *model=self.tableViewArray[indexPath.section];
    video.video_id=model.video_id;
    video.Htitle=model.video_name;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self presentViewController:video animated:YES completion:nil];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 9.5;
}


@end

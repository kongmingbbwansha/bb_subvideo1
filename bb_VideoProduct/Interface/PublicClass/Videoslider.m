//
//  Videoslider.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/22.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "Videoslider.h"

@implementation Videoslider


- (CGRect)trackRectForBounds:(CGRect)bounds
{
    return CGRectMake(0,(self.height - 4)/2,self.width, 4);
}

@end

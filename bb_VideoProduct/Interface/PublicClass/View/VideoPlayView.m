//
//  VideoPlayView.m
//  BabyPlay
//
//  Created by 符之飞 on 16/2/18.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "VideoPlayView.h"

@implementation VideoPlayView

+(Class)layerClass
{
    return [AVPlayerLayer class];
}

@end

//
//  ProgressView.m
//  
//
//  Created by 符之飞 on 16/3/24.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "ProgressView.h"

@interface ProgressView()

@property (nonatomic, strong) CAShapeLayer * shapeLayer;


/**
 * 圆环的宽度
 */
@property (nonatomic, assign) CGFloat         RingWidth;
/**
 * 进度条颜色
 */
@property (nonatomic, strong) UIColor      * UpdateProgressColor;
/**
 * 进度条背景颜色
 */
@property (nonatomic, strong) UIColor      * OldProgressColor;

/**
 * 1代表顺时针, 0代表逆时针
 */
@property (nonatomic, assign) NSInteger    WCorAN;


@end

@implementation ProgressView

- (instancetype)initWithANFrame:(CGRect)frame BackColor:(UIColor *)backColor RingWidth:(CGFloat)ringWidth UpdateProgressColor:(UIColor *)updateProgressColor OldProgressColor:(UIColor *)oldProgressColor
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _WCorAN = 0;
        _RingWidth = ringWidth;
        _UpdateProgressColor = updateProgressColor;
        _OldProgressColor = oldProgressColor;
        self.backgroundColor = [UIColor clearColor];
        [self backShapelayer];
        [self ANCAShapeLayer];
        self.backimage = [[UIImageView alloc]initWithFrame:CGRectMake(ringWidth + ringWidth/2, ringWidth + ringWidth/2, self.frame.size.width - ringWidth*3, self.frame.size.width - ringWidth*3)];
        self.backimage.layer.masksToBounds = YES;
        self.backimage.layer.cornerRadius = (self.frame.size.width - ringWidth*3) /2;
        self.backimage.backgroundColor = backColor;
        [self addSubview:self.backimage];
    }
    return self;
}

- (instancetype)initWithCWFrame:(CGRect)frame BackColor:(UIColor *)backColor RingWidth:(CGFloat)ringWidth UpdateProgressColor:(UIColor *)updateProgressColor OldProgressColor:(UIColor *)oldProgressColor 
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _WCorAN = 1;
        _RingWidth = ringWidth;
        _UpdateProgressColor = updateProgressColor;
        _OldProgressColor = oldProgressColor;
        self.backgroundColor = [UIColor clearColor];
        [self backShapelayer];
        [self CWCAShapeLayer];
        self.backimage = [[UIImageView alloc]initWithFrame:CGRectMake(ringWidth + ringWidth/2, ringWidth + ringWidth/2, self.frame.size.width - ringWidth*3, self.frame.size.width - ringWidth*3)];
        self.backimage.layer.masksToBounds = YES;
        self.backimage.layer.cornerRadius = (self.frame.size.width - ringWidth*3) /2;
        self.backimage.backgroundColor = backColor;
        [self addSubview:self.backimage];
    }
    return self;
}

-(void)CWCAShapeLayer
{
    //创建出CAShapeLayer
    self.shapeLayer = [CAShapeLayer layer];
    self.shapeLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    //曲线带有圆角
    self.shapeLayer.lineCap = @"round";
    //填充颜色为ClearColor
    self.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    //设置线条的宽度和颜色
    self.shapeLayer.lineWidth = _RingWidth;
    self.shapeLayer.strokeColor = _UpdateProgressColor.CGColor;
    //创建出圆形贝塞尔曲线
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2)
                                                        radius:self.frame.size.width/2 - _RingWidth
                                                    startAngle:-M_PI/2
                                                      endAngle:M_PI/2 * 3
                                                     clockwise:YES];
    //让贝塞尔曲线与CAShapeLayer产生联系
    self.shapeLayer.path = path.CGPath;
    self.shapeLayer.strokeStart = 0;
    self.shapeLayer.strokeEnd = 0;
    //添加并显示
    [self.layer addSublayer:self.shapeLayer];
}

-(void)ANCAShapeLayer
{
    //创建出CAShapeLayer
    self.shapeLayer = [CAShapeLayer layer];
    self.shapeLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    //曲线带有圆角
    self.shapeLayer.lineCap = @"round";
    //填充颜色为ClearColor
    self.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    //设置线条的宽度和颜色
    self.shapeLayer.lineWidth = _RingWidth;
    self.shapeLayer.strokeColor = _UpdateProgressColor.CGColor;
    //创建出圆形贝塞尔曲线
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2,self.frame.size.width/2)
                                                        radius:self.frame.size.width/2 - _RingWidth
                                                    startAngle:-M_PI/2
                                                      endAngle:M_PI/2 * 3
                                                     clockwise:YES];
    //让贝塞尔曲线与CAShapeLayer产生联系
    self.shapeLayer.path = path.CGPath;
    self.shapeLayer.strokeEnd = 1;
    self.shapeLayer.strokeStart = 1;
    //添加并显示
    [self.layer addSublayer:self.shapeLayer];

}


-(void)backShapelayer
{
    self.backgroundColor = [UIColor clearColor];
    //创建出CAShapeLayer
    CAShapeLayer * ShapeLayer = [CAShapeLayer layer];
    ShapeLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    //曲线带有圆角
    ShapeLayer.lineCap = @"round";
    //填充颜色为ClearColor
    ShapeLayer.fillColor = [UIColor clearColor].CGColor;
    //设置线条的宽度和颜色
    ShapeLayer.lineWidth = _RingWidth;
    ShapeLayer.strokeColor = _OldProgressColor.CGColor;
    //创建出圆形贝塞尔曲线
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2)
                                                        radius:self.frame.size.width/2 - _RingWidth
                                                    startAngle:-M_PI/2
                                                      endAngle:M_PI/2 * 3
                                                     clockwise:YES];
    //让贝塞尔曲线与CAShapeLayer产生联系
    ShapeLayer.path = path.CGPath;
    ShapeLayer.strokeStart = 0;
    ShapeLayer.strokeEnd = 1;
    //添加并显示
    [self.layer addSublayer:ShapeLayer];
}

-(void)setProgress:(CGFloat)Progress
{
    _Progress = Progress;
    if (_WCorAN == 1)
    {
        self.shapeLayer.strokeEnd = _Progress;
    }
    else
    {
        self.shapeLayer.strokeStart = 1 - _Progress;
    }
}


@end

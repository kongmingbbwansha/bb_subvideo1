//
//  EntryViewTableViewCell.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/20.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "EntryViewTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface EntryViewTableViewCell ()

/**
 * 背景
 */
@property (strong,nonatomic) UIImageView * backImageView;
/**
 * 左边图片
 */
@property (strong,nonatomic) UIImageView * RightImageView;
/**
 * 上标题
 */
@property (strong,nonatomic) UILabel * titleUp;
/**
 * 下标题
 */
@property (strong,nonatomic) UILabel * titlerDown;
/**
 * 右边图标
 */
@property (strong,nonatomic) UIImageView * LeftIconImageView;
@end

@implementation EntryViewTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self layoutView];
    }
    return self;
}
-(void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)layoutView
{
    self.contentView.backgroundColor=[UIColor whiteColor];
//    self.backImageView=[[UIImageView alloc]init];
//    self.backImageView.backgroundColor=[UIColor whiteColor];
    self.RightImageView=[[UIImageView alloc]init];
    self.titleUp=[[UILabel alloc]init];
    self.titleUp.font=[UIFont fontWithName:@"Helvetica-Bold" size:12.f];
    self.titlerDown=[[UILabel alloc]init];
    self.titlerDown.text=@"";
    self.titlerDown.font=[UIFont fontWithName:@"Helvetica-Bold" size:12];
    [self.titlerDown setTextColor:[UIColor colorWithRed:0.494 green:0.737 blue:0.000 alpha:1.000]];
    self.LeftIconImageView=[[UIImageView alloc]init];
    //设置位置
    [self.contentView addSubview:self.RightImageView];
    [self.contentView addSubview:self.LeftIconImageView];
    self.LeftIconImageView.sd_layout.topSpaceToView(self.contentView,10).leftSpaceToView(self.contentView,15).bottomSpaceToView(self.contentView,10).widthIs(105);
    self.RightImageView.sd_layout.topSpaceToView(self.contentView,55/2).bottomSpaceToView(self.contentView,55/2).widthIs(25).rightSpaceToView(self.contentView,15);
    self.RightImageView.image=[UIImage imageNamed:@"go@2x"];
    [self.contentView addSubview:self.titleUp];
    self.titleUp.sd_layout.topSpaceToView(self.contentView,15).bottomSpaceToView(self.contentView,45).leftSpaceToView(self.LeftIconImageView,15).rightSpaceToView(self.RightImageView,5);
    //显示所有字符，可选为省略开头或者省略结尾用.....的形式
   self.titleUp.lineBreakMode =NSLineBreakByCharWrapping;
    [self.contentView addSubview:self.titlerDown];
     self.titlerDown.sd_layout.topSpaceToView(self.contentView,45).bottomSpaceToView(self.contentView,15).leftSpaceToView(self.LeftIconImageView,15).rightSpaceToView(self.RightImageView,15);
}
-(void)DataPus:(EntryModel *)model
{
    self.titleUp.text=model.video_name;
    [self.LeftIconImageView sd_setImageWithURL:[NSURL URLWithString:model.video_picurl]];
    self.titlerDown.text= [NSString stringWithFormat:@"时长 %@",[self Lookvideotimr:[model.video_length integerValue]]];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
/**
 * 对时长进行处理 00：00 样式
 */
-(NSString *)Lookvideotimr:(NSInteger )timer
{
    NSString * AllTime;
    NSInteger time = timer;
    if ((time/60000 < 10 && time/60!=0) || time < 10)
    {
        AllTime = [NSString stringWithFormat:@"0%ld:%ld",(long)time/60000 ,(long)(time-(time/60000 *60000))/1000];
    }
    else
    {
        AllTime = [NSString stringWithFormat:@"%ld:%ld",(long)time/60000 ,(long)(time-(time/60000 *60000))/1000];
    }
    return AllTime;
}

@end

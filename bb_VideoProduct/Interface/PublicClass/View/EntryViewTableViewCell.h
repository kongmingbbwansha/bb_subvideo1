//
//  EntryViewTableViewCell.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/20.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EntryModel.h"

@interface EntryViewTableViewCell : UITableViewCell
-(void)DataPus:(EntryModel *)model;
@end

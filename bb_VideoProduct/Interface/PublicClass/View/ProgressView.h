//
//  ProgressView.h
//  新项目框架
//
//  Created by 符之飞 on 16/3/24.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressView : UIView

/**
 * 逆时针初始化 参数:BackColor 圆环内的背景颜色
 *                RingWidth 圆环宽度
 *                UpdateProgressColor 更新的圆环颜色
 *                OldProgressColor    圆环本身背景颜色
 */
- (instancetype)initWithANFrame:(CGRect)frame BackColor:(UIColor *)backColor RingWidth:(CGFloat)ringWidth UpdateProgressColor:(UIColor *)updateProgressColor OldProgressColor:(UIColor *)oldProgressColor;

/**
 * 顺时针初始化 参数:BackColor 环内背景颜色
 *                RingWidth 圆环宽度
 *                UpdateProgressColor 更新的圆环颜色
 *                OldProgressColor    圆环本身背景颜色
 */
- (instancetype)initWithCWFrame:(CGRect)frame BackColor:(UIColor *)backColor RingWidth:(CGFloat)ringWidth UpdateProgressColor:(UIColor *)updateProgressColor OldProgressColor:(UIColor *)oldProgressColor;

/**
 * 背景图
 */
@property (nonatomic, strong) UIImageView  * backimage;

/**
 * 跟新进度
 */
@property (assign, nonatomic) CGFloat    Progress;


@end

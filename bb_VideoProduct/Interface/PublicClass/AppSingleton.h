//
//  AppSingleton.h
//  bb_Picturebook
//
//  Created by 符之飞 on 16/3/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GDTMobInterstitial.h"
@interface AppSingleton : NSObject<GDTMobInterstitialDelegate>

/**
 * 初始化方法
 */
+(AppSingleton *)initAppsingleton;

@property(copy, nonatomic) NSString * app_id;
@property(copy, nonatomic) NSString *app_sub_id;
/**
 * 监听是否有广告
 */
@property (assign, nonatomic) NSInteger     insetadvert;
/**
 * 判断广告是否加载完成
 */
@property (assign, nonatomic) NSInteger     haveWithAdevert;
/**
 * 插屏
 */
@property (strong, nonatomic) GDTMobInterstitial*  interstitialObj;
-(void)AdvertPrestrain;
@end

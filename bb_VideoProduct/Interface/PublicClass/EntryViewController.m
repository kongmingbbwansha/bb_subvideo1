//
//  EntryViewController.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/21.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "EntryViewController.h"
#import "EntryViewTableViewCell.h"
#import "EntryRequest.h"
#import "EntryModel.h"
#import "EntryVideoController.h"
#import "AppSingleton.h"
#import "EntryKey.h"
#import "EntryKenModel.h"
#import "UIImageView+WebCache.h"
@interface EntryViewController ()<UITableViewDataSource,UITableViewDelegate>

/**
 * tableView
 */
@property(strong,nonatomic) UITableView *    tableView;
/**
 * 悬浮广告的图片
 */
@property(strong,nonatomic) UIImageView *     suspend;
/**
 * tableView的赋值数组
 */
@property(copy,nonatomic)   NSMutableArray *  tableViewArray;
/**
 * 加载数据的参数
 */
@property(assign,nonatomic) int page;
/**
 * 删除广告的按钮
 */
@property(strong,nonatomic) UIButton *        remove;
/**
 * 网络获取广告的参数字典
 */
@property(copy,nonatomic)   NSDictionary *    DicKey;

@end

@implementation EntryViewController
-(NSMutableArray *)tableViewArray
{
    if (_tableViewArray==nil) {
        _tableViewArray=[NSMutableArray array];
        self.page=1;
    }
    return _tableViewArray;
}
-(NSDictionary *)DicKey
{
    if (_DicKey==nil) {
        _DicKey=[[NSDictionary alloc]init];
    }
    return _DicKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor=[UIColor lightGrayColor];
    self.title=@"高一";
    [self layout];
    [self GET_Data];
    [self GET_key];
}
#pragma mark - 布局
-(void)layout
{
    
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
    [self.view addSubview:self.tableView];
    self.tableView.sd_layout.topSpaceToView(self.navgationbar,0).bottomSpaceToView(self.view,49).widthIs(self.view.frame.size.width);
    self.tableView.frame=CGRectMake(0, 64, self.view.width, self.view.height-103);
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.tableView.backgroundColor=[UIColor colorWithWhite:0.933 alpha:1.000];
    //下拉了刷新
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(GET_Data)];
    self.tableView.mj_header = header;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(GET_tagMore)];
    [footer setTitle:@"没有更多内容了" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;

}
#pragma mark - 广告的处理
/**
 * 判断广告状态
 */
-(void)KyeIsORno
{
    NSString *str=[NSString stringWithFormat:@"%@",[self.DicKey objectForKey:@"enable"]];
   
    if ([str isEqualToString:@"1"])
    {
        
        [self creatGG];
    }
    
}
/**
 * 广告的操作
 */
-(void)creatGG
{
    //广告悬浮图片
    self.suspend =[[UIImageView alloc]init];
    [self.view addSubview:self.suspend];
    self.suspend.sd_layout.leftSpaceToView(self.view,self.view.width/6).rightSpaceToView(self.view,self.view.width/6).topSpaceToView(self.navgationbar,20).heightIs(80);
    [self.suspend sd_setImageWithURL:[NSURL URLWithString:[self.DicKey objectForKey:@"coverurl"]]];
    self.suspend.userInteractionEnabled=YES;
    [self.view bringSubviewToFront:self.suspend];
    UITapGestureRecognizer *gest=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(skipWithAPPstor)];
    //    [self.suspend addTarget:self action:@selector(skipWithAPPstor) forControlEvents:UIControlEventTouchUpInside];
    gest.numberOfTapsRequired=1;
    [self.suspend addGestureRecognizer:gest];
    
    //关闭按钮
    self.remove=[[UIButton alloc]init];
    [self.suspend addSubview:self.remove];
    self.remove.sd_layout.topSpaceToView(self.suspend,0).rightSpaceToView(self.suspend,0).heightIs(20).widthIs(20);
    
    [self.remove setImage:[UIImage imageNamed:@"icon_close"] forState:UIControlStateNormal];
    [self.remove addTarget:self action:@selector(removeObjectIsBtn) forControlEvents:UIControlEventTouchUpInside];
}
/**
 * 关闭广告
 */
-(void)removeObjectIsBtn
{

    self.suspend.hidden=YES;
}
/**
 * 点击广告跳转
 */
-(void)skipWithAPPstor
{
   if ([[self.DicKey objectForKey:@"type"] isEqualToString:@"qqgroup"]) {
        [self joinGroup:[self.DicKey objectForKey:@"uin"] key:[self.DicKey objectForKey:@"key"]];
    }
    if ([[self.DicKey objectForKey:@"type"]  isEqualToString:@"weblink"]) {

        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[self.DicKey objectForKey:@"url"]]];
    }
}
#pragma mark - 加载数据
/**
 * 点击广告数据加载
 */
-(void)GET_key
{
    EntryKey *entry=[[EntryKey alloc]initWithRequest:GET_Key];
    
    entry.app_id=app_id;
    entry.app_sub_id=app_sub_id;
    __weak __typeof (self)weakself = self;
    [entry GetConectwithSucceed:^(id object)
    {
        
        __strong __typeof(self)strongself = weakself;
        if (SSUCCESS(object))
        {
            strongself.DicKey=[object objectForKey:@"data"];
            [self KyeIsORno];
            NSLog(@"docley=%@",self.DicKey);
        }

    } Failure:^(NSError *failure)
    {
        
    }];
    [self KyeIsORno];
}
/**
  * 加载数据刷新数据
  */
-(void)GET_Data
{
    self.page=1;
    EntryRequest *entry=[[EntryRequest alloc]initWithRequest:GET_subapp];
    entry.tab=@"1";
    entry.app_id=app_id;
    entry.app_sub_id=app_sub_id;
    entry.page=[NSString stringWithFormat:@"%d",self.page];
    __weak __typeof (self)weakself = self;
    [entry GetConectwithSucceed:^(id object)
     {
         __strong __typeof(self)strongself = weakself;
         [strongself.tableView.mj_header endRefreshing];
         if (SSUCCESS(object))
         {
             [strongself BannerObjectdata:[object objectForKey:@"data"]];
         }
         else
         {
             
         }
     }Failure:^(NSError *failure){
         __strong __typeof(self)strongself = weakself;
         [strongself.tableView.mj_header endRefreshing];
     }];
    
    
}
/**
 * 加载更多数据
 */
-(void)GET_tagMore
{
    self.page++;
    EntryRequest *entry=[[EntryRequest alloc]initWithRequest:GET_subapp];
    entry.tab=@"1";
    entry.app_id=app_id;
    entry.app_sub_id=app_sub_id;
    
    entry.page=[NSString stringWithFormat:@"%d",self.page];
    __weak __typeof (self)weakself = self;
    [entry GetConectwithSucceed:^(id object)
    {
        
        __strong __typeof(self)strongself = weakself;
        if (SSUCCESS(object))
        {
            [strongself ListObjectdataMore:[object objectForKey:@"data"]];
            
        }
 
    }
                        Failure:^(NSError *failure)
    {
        __strong __typeof(self)strongself = weakself;
        [strongself.tableView.mj_footer endRefreshing];
    }];

}
/**
 * 加载更多数据的赋值
 */
-(void)ListObjectdataMore:(NSArray *)array
{
    if (array.count != 0)
    {
        for (NSDictionary * dic in array)
        {
            EntryModel * model = [[EntryModel alloc]initWithDic:dic];
            [self.tableViewArray addObject:model];
        }
        [self.tableView.mj_footer endRefreshing];
        [self.tableView reloadData];
        
    }
    
    else{
        //如果没有数据停用上拉刷新，显示为没有内容
        self.tableView.mj_footer.state = MJRefreshStateNoMoreData;
        [self.tableView reloadData];
    }
    
}


/**
 * 刷新数据的赋值
 */
-(void)BannerObjectdata:(NSArray *)array
{
    [self.tableViewArray removeAllObjects];
    for (NSDictionary * dic in array)
    {
        EntryModel * model = [[EntryModel alloc]initWithDic:dic];
        [self.tableViewArray addObject:model];
    }
    //下拉刷新重置上拉刷新的状态
    self.tableView.mj_footer.state=MJRefreshStateIdle;
    [self.tableView reloadData];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - tabview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableViewArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EntryViewTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"zj"];
    if (cell==nil) {
        cell= [[EntryViewTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"zj"];
    }
    EntryModel *model=self.tableViewArray[indexPath.section];
    [cell DataPus:model];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
       
    EntryVideoController *video=[[EntryVideoController alloc]init];
    //取消选中状态
    EntryModel *model=self.tableViewArray[indexPath.section];
    video.video_id=model.video_id;
    video.Htitle=model.video_name;
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self presentViewController:video animated:YES completion:nil];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 9.5;
}
/**
 * 跳转QQ群
 */
- (BOOL)joinGroup:(NSString *)groupUin key:(NSString *)key{
    NSString *urlStr = [NSString stringWithFormat:@"mqqapi://card/show_pslcard?src_type=internal&version=1&uin=%@&key=%@&card_type=group&source=external", groupUin,key];
    NSURL *url = [NSURL URLWithString:urlStr];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
        return YES;
    }
    
    else
        
    {
        UIAlertController *alear=[UIAlertController alertControllerWithTitle:@"提示" message:@"您的手机没有装QQ软件" preferredStyle:UIAlertControllerStyleAlert];
        [alear addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }]];
        [self presentModalViewController:alear animated:YES];
        
        return NO;
    }
}

@end

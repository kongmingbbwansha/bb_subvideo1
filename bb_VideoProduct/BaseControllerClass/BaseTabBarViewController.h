//
//  BaseTabBarViewController.h
//  bb_Picturebook
//
//  Created by 符之飞 on 16/3/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TabBarView;

@interface BaseTabBarViewController : UITabBarController

@property (nonatomic, strong) TabBarView *tabBarView;


@end

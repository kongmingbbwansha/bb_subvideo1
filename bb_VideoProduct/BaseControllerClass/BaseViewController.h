//
//  BaseViewController.h
//  新项目框架
//
//  Created by 符之飞 on 16/3/23.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController<UIGestureRecognizerDelegate>

/**
 * 自定义导航栏
 */
@property (strong, nonatomic) UIView  * navgationbar;

/**
 * 导航栏背景图
 */
@property (strong, nonatomic) UIImageView * navgationimage;

/**
 * 禁止侧滑返回
 */
@property (assign, nonatomic)  BOOL   ScrollBack;

/**
 * 网络类型
 */
@property (assign, nonatomic) NSInteger   Nettype;

@end

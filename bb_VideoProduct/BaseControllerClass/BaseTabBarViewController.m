//
//  BaseTabBarViewController.m
//  bb_Picturebook
//
//  Created by 符之飞 on 16/3/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "BaseTabBarViewController.h"
#import "TabBarView.h"
#import "EntryViewController.h"
#import "InitialViewController.h"
#import "InetermadiateViewController.h"
#import "SeniorViewController.h"
@interface BaseTabBarViewController ()


@end

@implementation BaseTabBarViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
 
        
        self.tabBar.hidden = YES;
        [self.tabBar removeFromSuperview];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tabBarView = [[TabBarView alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-49, self.view.width, 49)];
    
    [self.view addSubview:_tabBarView];
    
     [self.tabBarView.EntryButton addTarget:self action:@selector(tabbarSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
     [self.tabBarView.InitialButton addTarget:self action:@selector(tabbarSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
     [self.tabBarView.IntermediateButton addTarget:self action:@selector(tabbarSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
     [self.tabBarView.SeniorButton addTarget:self action:@selector(tabbarSelectAction:) forControlEvents:UIControlEventTouchUpInside];
    
     self.selectedIndex=0;
    self.tabBarView.EntryImageView.backgroundColor=[UIColor colorWithRed:0.494 green:0.745 blue:0.000 alpha:1.000];
    EntryViewController * entry=[[EntryViewController alloc]init];
    InitialViewController * Initia=[[InitialViewController alloc]init];
    InetermadiateViewController *inete=[[InetermadiateViewController alloc]init];
    SeniorViewController *senior=[[SeniorViewController alloc]init];
    if (tabbariemNumber==3)
    {
        self.viewControllers=[NSArray arrayWithObjects:entry,Initia,inete, nil];
    }
    if (tabbariemNumber==4) {
        self.viewControllers=[NSArray arrayWithObjects:entry,Initia,inete,senior,nil];
    }
    
}

-(void)tabbarSelectAction: (UIButton *)tab
{
//通过tag值来区别点击的按钮,跳转并改变按钮背景颜色
    if (tab.tag==10086) {
        self.selectedIndex=0;
        self.tabBarView.EntryImageView.backgroundColor=[UIColor colorWithRed:0.494 green:0.745 blue:0.000 alpha:1.000];
        self.tabBarView.InitialImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.IntermediateImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.SeniorthImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
    };
    if (tab.tag==10087) {
        self.selectedIndex=1;
        self.tabBarView.EntryImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.InitialImageView.backgroundColor=[UIColor colorWithRed:0.494 green:0.745 blue:0.000 alpha:1.000];
        self.tabBarView.IntermediateImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.SeniorthImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];

    }
    if (tab.tag==10088) {
        self.selectedIndex=2;
        self.tabBarView.EntryImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.InitialImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.IntermediateImageView.backgroundColor=[UIColor colorWithRed:0.494 green:0.745 blue:0.000 alpha:1.000];
        self.tabBarView.SeniorthImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];

    }
    if (tab.tag==10089) {
        self.selectedIndex=3;
        self.tabBarView.EntryImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.InitialImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.IntermediateImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.tabBarView.SeniorthImageView.backgroundColor=[UIColor colorWithRed:0.494 green:0.745 blue:0.000 alpha:1.000];

    }
}









@end

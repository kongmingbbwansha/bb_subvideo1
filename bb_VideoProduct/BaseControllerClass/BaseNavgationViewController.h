//
//  BaseNavgationViewController.h
//  乐来乐爱
//
//  Created by 符之飞 on 15/1/22.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavgationViewController : UINavigationController<UINavigationControllerDelegate>

@end

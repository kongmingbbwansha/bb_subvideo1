//
//  TabBarView.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/13.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "TabBarView.h"

@implementation TabBarView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self layoutView];
    }
    return self;
}

/**
 *   布局
 */
- (void)layoutView

{
    //背景图片
    self.backimage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [self.backimage setBackgroundColor:[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:0.000]];
    self.backimage.userInteractionEnabled=YES;
    [self addSubview:self.backimage];
    //入门
    self.EntryImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width/tabbariemNumber, self.height)];
    self.EntryImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
    self.EntryImageView.userInteractionEnabled=YES;
    [self.backimage addSubview:self.EntryImageView];
    //创建一个imageView来放图标
    self.EntryIconImageView=[[UIImageView alloc]init];
    self.EntryLabel=[[UILabel alloc]init];
    [self.EntryImageView addSubview:self.EntryIconImageView];
    [self.EntryImageView addSubview:self.EntryLabel];

    self.EntryLabel.text=@"高一";
    self.EntryLabel.font=[UIFont fontWithName:@"Helvetica" size:10];
    [self.EntryLabel setTextColor:[UIColor whiteColor]];
    self.EntryLabel.textAlignment=1;
    self.EntryIconImageView.image=[UIImage imageNamed:@"m1@2x"];
    self.EntryIconImageView.sd_layout.topSpaceToView(self.EntryImageView,4).bottomSpaceToView(self.EntryImageView,22).leftSpaceToView(self.EntryImageView,(self.EntryImageView.width-23)/2).rightSpaceToView(self.EntryImageView,(self.EntryImageView.width-23)/2);
    self.EntryLabel.sd_layout.topSpaceToView(self.EntryImageView,31).bottomSpaceToView(self.EntryImageView,2).leftSpaceToView(self.EntryImageView,(self.EntryImageView.width-23)/2).rightSpaceToView(self.EntryImageView,(self.EntryImageView.width-23)/2);
      //创建一个透明按钮监听点击
    self.EntryButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.width/tabbariemNumber, self.height)];
    //取消按钮点击视觉效果
    self.EntryButton.adjustsImageWhenHighlighted = NO;
    self.EntryButton.tag=10086;
    [self.EntryButton setTitleColor:[UIColor colorWithWhite:0.995 alpha:1.000] forState:UIControlStateNormal];
    [self.EntryImageView addSubview:self.EntryButton];
    //初级
    self.InitialImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.width/tabbariemNumber, 0, self.width/tabbariemNumber, self.height)];
    self.InitialImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
    self.InitialImageView.userInteractionEnabled=YES;
    [self.backimage addSubview:self.InitialImageView];
    self.InitialIconImageView=[[UIImageView alloc]init];
    self.InitialLabel=[[UILabel alloc]init];
    [self.InitialImageView addSubview:self.InitialIconImageView];
    [self.InitialImageView addSubview:self.InitialLabel];
    self.InitialIconImageView.sd_layout.topSpaceToView(self.InitialImageView,4).bottomSpaceToView(self.InitialImageView,22).leftSpaceToView(self.InitialImageView,(self.InitialImageView.width-23)/2).rightSpaceToView(self.InitialImageView,(self.InitialImageView.width-23)/2);
    self.InitialLabel.sd_layout.topSpaceToView(self.InitialImageView,31).bottomSpaceToView(self.InitialImageView,2).leftSpaceToView(self.InitialImageView,(self.InitialImageView.width-23)/2).rightSpaceToView(self.InitialImageView,(self.InitialImageView.width-23)/2);
    self.InitialLabel.text=@"高二";
    self.InitialLabel.textAlignment=1;
    [self.InitialLabel setTextColor:[UIColor whiteColor]];
    self.InitialLabel.font=[UIFont fontWithName:@"Helvetica" size:10];
    self.InitialIconImageView.image=[UIImage imageNamed:@"m2@2x"];
    self.InitialButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.width/tabbariemNumber, self.height)];
    self.InitialButton.tag=10087;
    [self.InitialButton setTitleColor:[UIColor colorWithWhite:0.995 alpha:1.000] forState:UIControlStateNormal];
    [self.InitialImageView addSubview:self.InitialButton];

    //中级
    
    self.IntermediateImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.width/tabbariemNumber *2, 0, self.width/tabbariemNumber, self.height)];
    self.IntermediateImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
    self.IntermediateImageView.userInteractionEnabled=YES;
    [self.backimage addSubview:self.IntermediateImageView];
    self.IntermediateButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.width/tabbariemNumber, self.height)];
    self.IntermediateIconImageView=[[UIImageView alloc]init];
    self.IntermediateLabel=[[UILabel alloc]init];
     self.IntermediateLabel.textAlignment=1;
    [self.IntermediateImageView addSubview:self.IntermediateIconImageView];
    [self.IntermediateImageView addSubview:self.IntermediateLabel];
    self.IntermediateIconImageView.sd_layout.topSpaceToView(self.IntermediateImageView,4).bottomSpaceToView(self.IntermediateImageView,22).leftSpaceToView(self.IntermediateImageView,(self.IntermediateImageView.width-23)/2).rightSpaceToView(self.IntermediateImageView,(self.IntermediateImageView.width-23)/2);
    self.IntermediateLabel.sd_layout.topSpaceToView(self.IntermediateImageView,31).bottomSpaceToView(self.IntermediateImageView,2).leftSpaceToView(self.IntermediateImageView,(self.IntermediateImageView.width-23)/2).rightSpaceToView(self.IntermediateImageView,(self.IntermediateImageView.width-23)/2);
    self.IntermediateLabel.text=@"高三";
    self.IntermediateLabel.font=[UIFont fontWithName:@"Helvetica" size:10];
    [self.IntermediateLabel setTextColor:[UIColor whiteColor]];
    self.IntermediateIconImageView.image=[UIImage imageNamed:@"m3@2x"];
    self.IntermediateButton.tag=10088;
    [self.IntermediateButton setTitleColor:[UIColor colorWithWhite:0.995 alpha:1.000] forState:UIControlStateNormal];
    [self.IntermediateImageView addSubview:self.IntermediateButton];

    //高级
    if (tabbariemNumber == 4) {
        self.SeniorthImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.width/tabbariemNumber *3, 0, self.width/tabbariemNumber, self.height)];
        self.SeniorthImageView.backgroundColor=[UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
        self.SeniorthImageView.userInteractionEnabled=YES;
        [self.backimage addSubview:self.SeniorthImageView];
        self.SeniorButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.width/tabbariemNumber, self.height)];
        self.SeniorthIconImageView=[[UIImageView alloc]init];
        self.SeniorLabel=[[UILabel alloc]init];
        [self.SeniorthImageView addSubview:self.SeniorthIconImageView];
        [self.SeniorthImageView addSubview:self.SeniorLabel];
        self.SeniorLabel.text=@"高级";
        self.SeniorLabel.font=[UIFont fontWithName:@"Helvetica" size:10];
        [self.SeniorLabel setTextColor:[UIColor whiteColor]];
        self.SeniorthIconImageView.image=[UIImage imageNamed:@"3@2x"];
        self.SeniorLabel.textAlignment=1;
        self.SeniorthIconImageView.sd_layout.topSpaceToView(self.SeniorthImageView,4).bottomSpaceToView(self.SeniorthImageView,22).leftSpaceToView(self.SeniorthImageView,(self.SeniorthImageView.width-23)/2).rightSpaceToView(self.SeniorthImageView,(self.SeniorthImageView.width-23)/2);
        self.SeniorLabel.sd_layout.topSpaceToView(self.SeniorthImageView,31).bottomSpaceToView(self.SeniorthImageView,2).leftSpaceToView(self.SeniorthImageView,(self.SeniorthImageView.width-23)/2).rightSpaceToView(self.SeniorthImageView,(self.SeniorthImageView.width-23)/2);
        self.SeniorButton.tag=10089;
        [self.SeniorButton setTitleColor:[UIColor colorWithWhite:0.995 alpha:1.000] forState:UIControlStateNormal];
        [self.SeniorthImageView addSubview:self.SeniorButton];
        

    }
    }
@end

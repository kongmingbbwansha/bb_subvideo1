//
//  BaseViewController.m
//  新项目框架
//
//  Created by 符之飞 on 16/3/23.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@property (strong, nonatomic) UILabel   *titlename;

@end

@implementation BaseViewController

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self getNetWorkNotificationCenter];
        self.automaticallyAdjustsScrollViewInsets = NO;
        _ScrollBack = NO;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.navigationController.viewControllers.count == 1)
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    else
    {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

-(void)getNetWorkNotificationCenter
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         switch (status)
         {
             case AFNetworkReachabilityStatusNotReachable:
             {
                 [self NetworkReach:0];
                 break;
             }
             case AFNetworkReachabilityStatusReachableViaWWAN:
             {
                 [self NetworkReach:1];
                 break;
             }
             case AFNetworkReachabilityStatusReachableViaWiFi:
             {
                 [self NetworkReach:2];
                 break;
             }
             default:
                 break;
         }
     }];

}


#pragma mark - 当前网络状态
-(void)NetworkReach:(NSInteger )type
{
    self.Nettype = type;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //底部背景图片
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_home1"]];
    //自定义导航栏
    self.navgationbar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    self.navgationbar.backgroundColor = [UIColor colorWithRed:0.686 green:0.824 blue:0.275 alpha:1.000];
    [self.navgationbar setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    //导航栏背景图片
    self.navgationimage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.navgationbar.width, self.navgationbar.height - 0.5)];
    [self.navgationbar addSubview:self.navgationimage];
    [self.navgationimage setImage:[UIImage imageNamed:@"bg_home_tab"]];
    [self.navgationimage setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    //    线
    UIView * line = [[UIView alloc]initWithFrame:CGRectMake(0, self.navgationbar.bottom - 0.3, self.navgationbar.frame.size.width, 0.3)];
    line.backgroundColor = [UIColor colorWithWhite:0.745 alpha:1.000];
    [line setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.navgationbar addSubview:line];
    //标题
    self.titlename = [[UILabel alloc]initWithFrame:CGRectMake(65, 20, self.view.width - 130, self.navgationbar.height - 20)];
    [self.titlename setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    self.titlename.font = [UIFont systemFontOfSize:18];
    self.titlename.textColor = [UIColor whiteColor];
    self.titlename.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.navgationbar];
    [self.navgationbar addSubview:self.titlename];
}

#pragma mark - 内存警告释放资源
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache] clearDisk];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"%@ -> didReceiveMemoryWarning      Title -> %@",[self class],[self title]);

}

-(void)setTitle:(NSString *)title
{
    self.titlename.text = title;
}

#pragma mark - 侧滑返回
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.ScrollBack == YES)
    {
        return NO;
    }
    else
    {
        if ([self isRootViewController])
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
}

- (BOOL)isRootViewController
{
    return (self == self.navigationController.viewControllers.firstObject);
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return [gestureRecognizer isKindOfClass:UIScreenEdgePanGestureRecognizer.class];
    
}


-(BOOL)shouldAutorotate
{
    return NO;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    
    return YES;
    
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

@end

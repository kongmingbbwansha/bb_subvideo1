//
//  TabBarView.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/13.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarView : UIView

/**
 * tabbar
 */
@property(nonatomic, strong) UIImageView * backimage;

/**
 * 每个item的父视图
 */
@property (nonatomic, strong) UIImageView *EntryImageView;
/**
 * 图标
 */
@property (nonatomic, strong) UIImageView *EntryIconImageView;
/**
 * 透明按钮监听点击事件
 */
@property (nonatomic, strong) UIButton *EntryButton;
/**
 * 文字
 */
@property (nonatomic, strong) UILabel *EntryLabel;

@property (nonatomic, strong) UIImageView *InitialImageView;
@property (nonatomic, strong) UIImageView *InitialIconImageView;
@property (nonatomic, strong) UIButton *InitialButton;
@property (nonatomic, strong) UILabel *InitialLabel;

@property (nonatomic, strong) UIImageView *IntermediateImageView;
@property (nonatomic, strong) UIImageView *IntermediateIconImageView;
@property (nonatomic, strong) UIButton *IntermediateButton;
@property (nonatomic, strong) UILabel *IntermediateLabel;

@property (nonatomic, strong) UIImageView *SeniorthImageView;
@property (nonatomic, strong) UIImageView *SeniorthIconImageView;
@property (nonatomic, strong) UIButton *SeniorButton;
@property (nonatomic, strong) UILabel *SeniorLabel;







@end

//
//  HTTPfile.h
//  宝贝玩啥
//
//  Created by 符之飞 on 15/7/13.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#ifndef _____HTTPfile_h
#define _____HTTPfile_h
//广告的APPkey
static NSString *
Umeng=@"5721a9ebe0f55a17fd0005e1";
//广告的APPkey5721a9ebe0f55a17fd0005e1
static NSString *const APPkey=@"1105325032";
//开屏广告的placementId
static NSString *const OpenplacementId=@"4040918075604861";
//插屏广告的placementId
static NSString *const IensertplacementId=@"4020712085506852";
//网络请求的两个常量
static NSString *const app_id=@"101";
static NSString *const app_sub_id=@"1";
//tabbartiemNumber的个数
static int const tabbariemNumber = 3;

//首页列表(睡前)
static NSString *const GET_subapp = @"subapp";
static NSString *const GET_Key = @"subapp/adconfig";

//首页tag列表
static NSString * const GET_bannertags = @"tag/bannertags";

//根据tag显示列表信息
static NSString * const GET_tag = @"tag";

//发现接口
static NSString * const GET_ad = @"ad";

//发现详情列表
static NSString * const GET_ad_detail = @"ad/detail";

//获得一本书的详情
static NSString * const GET_book = @"book";

//上传宝贝信息
static NSString * const POST_user_kid = @"user/kid";

//伴读列表
static NSString * const GET_tag_parentpage = @"tag/parentpage";

//判断vip信息
static NSString * const GET_misc_be_vip = @"misc/be_vip";

//VIP接口
static NSString * const POST_vip = @"misc/vip_verify";

//搜索结果
static NSString * const GET_search = @"search";

//搜索热词
static NSString * const GET_search_hot = @"search/hot";

//添加播放次数
static NSString * const POST_book = @"book";

//获得广告配置
static NSString * const GET_misc_ad_config = @"misc/ad_config";

#endif

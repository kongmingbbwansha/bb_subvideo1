//
//  AppDelegate.h
//  bb_VideoProduct
//
//  Created by 符之飞 on 16/4/12.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GDTSplashAd.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,GDTSplashAdDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) GDTSplashAd *splash;
@end


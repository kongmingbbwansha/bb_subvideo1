//
//  UILabel+Adapt.h
//  宝贝玩啥
//
//  Created by 符之飞 on 15/7/17.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Adapt)

/**
 * 自适应高度
 */
- (void)boundingRectWithSize:(CGSize)size;


/**
 * 自适应宽度
 */
-(void)boundingRectWithSizewidth:(CGSize)size;

@end

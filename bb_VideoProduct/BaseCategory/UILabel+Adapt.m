//
//  UILabel+Adapt.m
//  宝贝玩啥
//
//  Created by 符之飞 on 15/7/17.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import "UILabel+Adapt.h"

@implementation UILabel (Adapt)

- (void)boundingRectWithSize:(CGSize)size
{
    if ([BaseControl isBlankString:self.text] == NO)
    {
        self.numberOfLines = 0;
        NSDictionary *attribute = @{NSFontAttributeName:self.font};
        CGSize retSize = [self.text boundingRectWithSize:size
                                                 options:\
                          NSStringDrawingTruncatesLastVisibleLine |
                          NSStringDrawingUsesLineFragmentOrigin |
                          NSStringDrawingUsesFontLeading
                                              attributes:attribute
                                                 context:nil].size;
        self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,self.frame.size.width, retSize.height);
    }
}

-(void)boundingRectWithSizewidth:(CGSize)size
{
    if ([BaseControl isBlankString:self.text] == NO)
    {
        self.numberOfLines = 0;
        NSDictionary *attribute = @{NSFontAttributeName:self.font};
        
        CGSize retSize = [self.text boundingRectWithSize:size
                                                 options:\
                          NSStringDrawingTruncatesLastVisibleLine |
                          NSStringDrawingUsesLineFragmentOrigin |
                          NSStringDrawingUsesFontLeading
                                              attributes:attribute
                                                 context:nil].size;
        self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y,retSize.width, self.frame.size.height);
    }
}

@end

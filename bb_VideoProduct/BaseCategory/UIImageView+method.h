//
//  UIImageView+method.h
//  宝贝玩啥
//
//  Created by 符之飞 on 15/7/11.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewExt.h"
@interface UIImageView (method)


/*
 *网络请求后的图片按比例放置到imageview中,但是要等到请求到图片后才能使用.
 */
-(void)ImageViewRatioSize;


-(CGRect)ImageViewwidth:(CGFloat )wight height:(CGFloat) height;

@end

//
//  UIImageView+method.m
//  宝贝玩啥
//
//  Created by 符之飞 on 15/7/11.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import "UIImageView+method.h"

@implementation UIImageView (method)


-(void)ImageViewRatioSize
{
    if (self.image.size.width != 0 || self.image.size.height != 0)
    {
        CGFloat oldWdith = self.width;
        CGFloat oldHeight = self.height;
        
        //图片放大的倍数
        CGFloat imgheight = 0.0;
        CGFloat imgwdith = 0.0;
        
        //如果是小图.按原型加载
        if (self.image.size.width < oldWdith && self.image.size.height < oldHeight)
        {
            self.width = self.image.size.width;
            self.height = self.image.size.height;
        }
        //如果原型的宽小于imageview的宽,长大于imageview的长.
        else if (self.image.size.width <= oldWdith && self.image.size.height > oldHeight)
        {
            imgheight = oldHeight/self.image.size.height;
            self.width = self.image.size.width * imgheight;
            self.height = oldHeight;
            
        }
        //如果原型的宽大于imageview的宽,长小于imageview的长.
        else if (self.image.size.width > oldWdith && self.image.size.height <= oldHeight)
        {
            imgwdith = oldWdith/self.image.size.width;
            self.width = oldWdith;
            self.height = self.image.size.height * imgwdith;
        }
        //如果原型的宽和高都大于imageview的宽高
        else if(self.image.size.width >= oldWdith && self.image.size.height >= oldHeight)
        {
            if(self.image.size.width/self.image.size.height >= oldWdith/oldHeight)
            {
                imgwdith = oldWdith/self.image.size.width;
                self.width = oldWdith;
                self.height = self.image.size.height * imgwdith;
            }
            else
            {
                imgheight = oldHeight/self.image.size.height;
                self.width = self.image.size.width * imgheight;
                self.height = oldHeight;
            }
        }
    
        //图片居中显示
        CGFloat  Wdith = self.image.size.width * imgwdith;
        CGFloat  Height = self.image.size.height * imgheight;
        CGFloat offsetX = (oldWdith > Wdith)?
        ((oldWdith - Wdith) * 0.5 + self.frame.origin.x) : self.frame.origin.x;
        CGFloat offsetY = ( oldHeight > Height)?
        ((oldHeight - Height) * 0.5 + self.frame.origin.y) : self.frame.origin.y;
        self.center = CGPointMake(Wdith * 0.5 + offsetX,
                                  Height * 0.5 + offsetY);
    }
}

-(CGRect)ImageViewwidth:(CGFloat )wight height:(CGFloat) height;
{
    CGRect  rect = CGRectMake(1, 1, 1, 1);
    if (self.image.size.width != 0 || self.image.size.height != 0)
    {
        CGFloat oldWdith = wight;
        CGFloat oldHeight = height;
        
        //图片放大的倍数
        CGFloat imgheight = 0.0;
        CGFloat imgwdith = 0.0;
        
        //如果是小图.按原型加载
        if (self.image.size.width < oldWdith && self.image.size.height < oldHeight)
        {
            rect.size.width = self.image.size.width;
            rect.size.height = self.image.size.height;
            
        }
        //如果原型的宽小于imageview的宽,长大于imageview的长.
        else if (self.image.size.width <= oldWdith && self.image.size.height > oldHeight)
        {
            imgheight = oldHeight/self.image.size.height;
            rect.size.width = self.image.size.width * imgheight;
            rect.size.height = oldHeight;
            
            
        }
        //如果原型的宽大于imageview的宽,长小于imageview的长.
        else if (self.image.size.width > oldWdith && self.image.size.height <= oldHeight)
        {
            imgwdith = oldWdith/self.image.size.width;
            rect.size.width = oldWdith;
            rect.size.height = self.image.size.height * imgwdith;
        }
        //如果原型的宽和高都大于imageview的宽高
        else if(self.image.size.width >= oldWdith && self.image.size.height >= oldHeight)
        {
            if(self.image.size.width/self.image.size.height >= oldWdith/oldHeight)
            {
                imgwdith = oldWdith/self.image.size.width;
                rect.size.width = oldWdith;
                rect.size.height = self.image.size.height * imgwdith;
                
            }
            else
            {
                imgheight = oldHeight/self.image.size.height;
                rect.size.width = self.image.size.width * imgheight;
                rect.size.height = oldHeight;
                
            }
        }
        return rect;
    }
    else
    {
        return rect;
    }
}

@end

//
//  SaveCachesFile.m
//  
//
//  Created by 符之飞 on 15/7/11.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import "SaveCachesFile.h"

@implementation SaveCachesFile

+ (id)loadDataList:(NSString *)fileName
{
    //  NSString *filePath = [NSHomeDirectory() stringByAppendingPathComponent:CacheName];
    
    //先拿到NSDocumentDirectory里的内容
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0]stringByAppendingPathComponent:FolderName];
    NSFileManager *manager = [[NSFileManager alloc]init];
    if (![manager fileExistsAtPath:path])
    {
        NSError *error ;
        [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        if (error)
        {
            
        }
    }
    NSString* fileDirectory = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.arc",fileName]];
    //解归档
    return [NSKeyedUnarchiver unarchiveObjectWithFile:fileDirectory];
}

+ (void)saveDataList:(id)object fileName:(NSString *)fileName
{
    //归档对象
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0]stringByAppendingPathComponent:FolderName];
    NSFileManager *manager = [[NSFileManager alloc]init];
    if (![manager fileExistsAtPath:path])
    {
        NSError *error ;
        [manager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
        if (error)
        {
            
        }
    }
    
    NSString* fileDirectory = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.arc",fileName]];
    
    BOOL success = [NSKeyedArchiver archiveRootObject:object toFile:fileDirectory];
    
    if (success)
    {
        
    }
    
}

+ (BOOL)removeFile:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0]stringByAppendingPathComponent:FolderName];
    NSFileManager *manager = [[NSFileManager alloc]init];
    if (![manager fileExistsAtPath:path])
    {
        return YES;
    }
    
    NSString* fileDirectory = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.arc",fileName]];
    
    BOOL success = [manager removeItemAtPath:fileDirectory error:nil];
    
    if (success)
    {
        NSLog(@"归档删除成功");
        return YES;
    }
    else
    {
        return NO;
    }
}



@end

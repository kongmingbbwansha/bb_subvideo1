//
//  SaveCachesFile.h
//  
//
//  Created by 符之飞 on 15/7/11.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FolderName @"feifei"

@interface SaveCachesFile : NSObject

/**
 *  解归档
 *
 *  @param fileName 文件名
 *
 *  @return 解完归档后的数据
 */
+ (id)loadDataList:(NSString *)fileName;

/**
 *  归档
 *
 *  @param object   归档的数据
 *  @param fileName 文件名
 */
+ (void)saveDataList:(id)object fileName:(NSString *)fileName;


/**
 *  删除归档
 *
 *  @param fileName 文件名
 */
+ (BOOL)removeFile:(NSString *)fileName;
@end

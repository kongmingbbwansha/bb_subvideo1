//
//  BaseControl.h
//  
//
//  Created by 符之飞 on 15/2/13.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonHMAC.h>
#import <CommonCrypto/CommonCryptor.h>
@interface BaseControl : NSObject

/**
 * 获取系统当前时间
 */
+(NSString *)NowTimer;

/**
 * 压缩图片
 */
+(UIImage *)scale:(UIImage *)image;

/**
 * 将带有中文字符的图片链接转化
 */
+(NSString *)URLImageEncodedString:(NSString *)string;

/**
 * 判断字符串是否为空,包括换行空格等
 */
+ (BOOL)isBlankString:(NSString *)string;

/**
 * 放大动画
 */
+(void)animationview:(UIView * )view;

/**
 * 判断是否是在当前控制器
 */
+(BOOL)isController:(UIViewController *)controller;

/**
 * 判断某一串字符串是否符合电话号码格式。YES:符合 NO:不符合
 */
+ (BOOL)isMobileNumber:(NSString *)mobileNum;

/**
 * 判断某一串字符串是否符合邮箱格式。YES:符合 NO:不符合
 */
+ (BOOL)isEmailNumber:(NSString *)string;

/**
 * 判断密码是否正确
 */
+(BOOL)CheckInput:(NSString *)_text;


/**
 *  图片MP3前缀
 */
+(NSString*)URLString:(NSString *)url;

/**
 *  获取视频URL
 */
+(NSString *)MP4URLvid:(NSString *)video_id;

@end

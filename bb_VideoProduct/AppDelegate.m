//
//  AppDelegate.m
//  bb_VideoProduct
//
//  Created by 符之飞 on 16/4/12.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "AppDelegate.h"
#import "BaseTabBarViewController.h"
#import "BaseNavgationViewController.h"


@interface AppDelegate ()
@property (strong, nonatomic) BaseNavgationViewController   * nav;

@property (strong, nonatomic) BaseTabBarViewController      * tabbar;



@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    BaseTabBarViewController *tabBarVC = [[BaseTabBarViewController alloc] init];
    
    BaseNavgationViewController * nav = [[BaseNavgationViewController alloc]initWithRootViewController:tabBarVC];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window.rootViewController = nav;
    
    //友盟统计
    [MobClick startWithAppkey:Umeng reportPolicy:BATCH channelId:@"App Store"];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];

    _splash = [[GDTSplashAd alloc] initWithAppkey:APPkey placementId:OpenplacementId];
    _splash.delegate = self;//设置代理
    //针对不同设备尺寸设置不同的默认图片，拉取广告等待时间会展示该默认图片。
    if ([[UIScreen mainScreen] bounds].size.height >= 568.0f) {
        _splash.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"640-960@2x"]];
        
        } else {
            
        _splash.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"750-1334@2x"]];
        
    }
    
    UIWindow *fK = [[UIApplication sharedApplication] keyWindow];
    //设置开屏拉取时长限制，若超时则不再展示广告
    _splash.fetchDelay = 3;
    //拉取并展示
    [_splash loadAdAndShowInWindow:fK];
  
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    //进去后台
     [[NSNotificationCenter defaultCenter] postNotificationName:@"stop" object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //回台前台
    [[NSNotificationCenter defaultCenter] postNotificationName:@"play" object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 开屏广告成功展示
-(void)splashAdSuccessPresentScreen:(GDTSplashAd *)splashAd
{
    NSLog(@"开屏广告成功展示");
    
    [[AppSingleton initAppsingleton] AdvertPrestrain];
}

#pragma mark - 开屏广告展示失败
-(void)splashAdFailToPresent:(GDTSplashAd *)splashAd withError:(NSError *)error
{    NSLog(@"开屏广告展示失败");
     [[AppSingleton initAppsingleton] AdvertPrestrain];
}

#pragma mark - 应用进入后台时回调
- (void)splashAdApplicationWillEnterBackground:(GDTSplashAd *)splashAd
{
    NSLog(@"应用进入后台时回调");
}

#pragma mark - 开屏广告点击回调
- (void)splashAdClicked:(GDTSplashAd *)splashAd
{
    NSLog(@"开屏广告点击回调");
}

#pragma mark - 开屏广告关闭回调
- (void)splashAdClosed:(GDTSplashAd *)splashAd
{
    NSLog(@"开屏广告关闭回调");
  }
/**
 *  开屏广告点击以后即将弹出全屏广告页
 */
- (void)splashAdWillPresentFullScreenModal:(GDTSplashAd *)splashAd
{
    
}
/**
 *  点击以后全屏广告页已经被关闭
 */
- (void)splashAdDidDismissFullScreenModal:(GDTSplashAd *)splashAd
{
    
}


@end

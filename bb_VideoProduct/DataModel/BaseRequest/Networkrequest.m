//
//  Networkrequest.m
//  
//
//  Created by 符之飞 on 15/1/21.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import "Networkrequest.h"

#define URLHTTP @"http://api.bbwansha.com:8083/bb_video20/index.php/Home/"



@interface Networkrequest()

@property (nonatomic, strong)NSString * URL;//网址前缀

@end

@implementation Networkrequest

- (instancetype)initWithRequest:(NSString * )urlRequest
{
    self = [super init];
    if (self)
    {
        _URL = urlRequest;
        //[self getNetWorkNotificationCenter];
    }
    return self;
}

//POST参数为FromData
-(NSMutableDictionary * )FromDataRequestParameter
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    return dic;
}

-(NSMutableDictionary * )QueryRequestParameter
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    return dic;
}

#pragma mark - Get请求
-(void)GetConectwithSucceed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    manager.requestSerializer.timeoutInterval = 20;//网络请求超出时间
    self.URL = [self dicrequest];
    NSLog(@"请求网址为%@",self.URL);
    [manager GET:self.URL parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"%lld", downloadProgress.totalUnitCount);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        succeed(responseObject);
        NSLog(@"请求回来的数据%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"请求回来的数据%@",error);
        failure(error);
        
    }];
}

-(void)GetConectwithHTTPURL:(NSString *)URL Succeed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain",nil];
    manager.requestSerializer.timeoutInterval = 20;
    self.URL = [self dicrequestHTTPURL:URL];
    NSLog(@"请求网址为%@",self.URL);
    [manager GET:self.URL parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        NSLog(@"%lld", downloadProgress.totalUnitCount);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         succeed(responseObject);
         NSLog(@"请求回来的数据%@",responseObject);
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
     {
         NSLog(@"请求回来的数据%@",error);
         failure(error);
         
     }];
}

#pragma mark - Post请求
-(void)PostConectwithSucceed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure;
{
    NSLog(@"%@",[self dicrequest]);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //设置接收的数据类型
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    manager.requestSerializer.timeoutInterval = 20;
    //这个方法请求到的数据不需要解析
    [manager POST:[self dicrequest] parameters:[self FromDataRequestParameter] progress:^(NSProgress * _Nonnull uploadProgress)
    {
        NSLog(@"%lld", uploadProgress.totalUnitCount);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        succeed(responseObject);
        NSLog(@"返回数据是%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
        NSLog(@"请求发生错误,错误信息为:%@",error);
    }];
}

//网址拼接,获取的网址,统一的前缀
-(NSString *)dicrequest
{
    NSDictionary * dic = [self QueryRequestParameter];
    NSString *value = @"";
    for (NSString *key in [dic allKeys])
    {
        if ([value length] == 0)
        {
            value = [NSString stringWithFormat:@"%@=%@",key,[dic objectForKey:key]];
        }
        else
        {
            value = [NSString stringWithFormat:@"%@&%@=%@",value,key,[dic objectForKey:key]];
        }
    }
    NSString * str = [NSString stringWithFormat:@"%@%@?%@",URLHTTP,_URL,value];
    return str;
}

//网址拼接,获取的网址,自定义的前缀
-(NSString *)dicrequestHTTPURL:(NSString*)url
{
    NSDictionary * dic = [self QueryRequestParameter];
    NSString *value = @"";
    for (NSString *key in [dic allKeys])
    {
        if ([value length] == 0)
        {
            value = [NSString stringWithFormat:@"%@=%@",key,[dic objectForKey:key]];
        }
        else
        {
            value = [NSString stringWithFormat:@"%@&%@=%@",value,key,[dic objectForKey:key]];
        }
    }
    NSString * str = [NSString stringWithFormat:@"%@%@?%@",url,_URL,value];
    return str;
}

@end

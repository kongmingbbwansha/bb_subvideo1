//
//  EntryKey.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "EntryKey.h"

@implementation EntryKey
-(NSMutableDictionary * )QueryRequestParameter
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    
    [dic setValue:self.app_id forKey:@"app_id"];
    [dic setValue:self.app_sub_id forKey:@"app_sub_id"];
    
    return dic;
}

@end


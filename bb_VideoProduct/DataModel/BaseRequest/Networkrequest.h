//
//  Networkrequest.h
//
//
//  Created by 符之飞 on 15/1/21.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Networkrequest : NSObject


/**
 *  传入的接口
 */
- (instancetype)initWithRequest:(NSString * )urlRequest;

/**
 *  Get请求统一前缀
 */
-(void)GetConectwithSucceed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure;

/**
 *  Get请求自定义前缀
 */
-(void)GetConectwithHTTPURL:(NSString *)URL Succeed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure;

/**
 *  post请求统一前缀
 */
-(void)PostConectwithSucceed:(void(^)(id object))succeed Failure:(void(^)(NSError * failure))failure;

@end

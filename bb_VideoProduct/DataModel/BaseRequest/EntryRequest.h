//
//  EntryRequest.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/20.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "Networkrequest.h"

@interface EntryRequest : Networkrequest

@property(copy,nonatomic) NSString *tab;
@property(copy,nonatomic) NSString *app_id;
@property(copy,nonatomic) NSString *app_sub_id;
@property(copy,nonatomic) NSString *page;
@end

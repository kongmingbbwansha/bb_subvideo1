//
//  EntryKey.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "Networkrequest.h"

@interface EntryKey : Networkrequest
@property(copy,nonatomic) NSString *app_id;
@property(copy,nonatomic) NSString *app_sub_id;
@end

//
//  EntryRequest.m
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/20.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "EntryRequest.h"

@implementation EntryRequest

-(NSMutableDictionary * )QueryRequestParameter
{
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    [dic setValue:self.tab forKey:@"tab"];
    [dic setValue:self.app_id forKey:@"app_id"];
    [dic setValue:self.app_sub_id forKey:@"app_sub_id"];
    [dic setValue:self.page forKey:@"page"];
    return dic;
}

@end

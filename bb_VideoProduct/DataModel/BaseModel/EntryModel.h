//
//  EntryModel.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/20.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "BaseModel.h"

@interface EntryModel : BaseModel
@property (copy, nonatomic) NSString * video_id;
@property (copy, nonatomic) NSString * video_name;
@property (copy, nonatomic) NSString * video_length;
@property (copy, nonatomic) NSString * video_size;
@property (copy, nonatomic) NSString * video_vip_flag;
@property (copy, nonatomic) NSString * video_play_count;
@property (copy, nonatomic) NSString * video_excerpt;
@property (copy, nonatomic) NSString * video_picurl;
@property (copy, nonatomic) NSString * video_series_id;
@property (copy, nonatomic) NSString * video_series_order;
@property (copy, nonatomic) NSString * video_mp4url;
@property (copy, nonatomic) NSString * video_qiniu_cdn_id;
@property (copy, nonatomic) NSString * video_youku_vid;
@property (copy, nonatomic) NSString * video_age_begin;
@property (copy, nonatomic) NSString * video_age_end;
@end

//
//  EntryKenModel.h
//  bb_VideoProduct
//
//  Created by bbwansha on 16/4/25.
//  Copyright © 2016年 符之飞. All rights reserved.
//

#import "BaseModel.h"

@interface EntryKenModel : BaseModel
@property (copy, nonatomic) NSString * enable;
@property (copy, nonatomic) NSString * type;
@property (copy, nonatomic) NSString * url;
@property (copy, nonatomic) NSString * uin;
@property (copy, nonatomic) NSString * key;
@end

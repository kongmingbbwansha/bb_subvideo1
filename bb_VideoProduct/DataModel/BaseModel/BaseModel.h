//
//  BaseModel.h
//  
//
//  Created by 符之飞 on 15/3/5.
//  Copyright (c) 2015年 符之飞. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
@interface BaseModel : NSObject

- (instancetype)initWithDic:(NSDictionary *)dic;

@end
